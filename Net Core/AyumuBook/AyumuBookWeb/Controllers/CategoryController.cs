﻿using AyumuBookWeb.Data;
using AyumuBookWeb.Models;
using Microsoft.AspNetCore.Mvc;

namespace AyumuBookWeb.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ApplicationDbContext _db;
        public CategoryController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            IEnumerable<Category> objCategoria = _db.Categoriee.ToList();
            return View(objCategoria);
        }
        //Get
        public IActionResult Create()
        {
            return View();
        }
        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Category categoryObj)
        {
            if(categoryObj.Name == categoryObj.DisplayOrder.ToString())
            {
                ModelState.AddModelError("CustomError", "La orden no puede ser exacto como el nombre");
            }
            if (ModelState.IsValid)
            {
                _db.Categoriee.Add(categoryObj);
                _db.SaveChanges();
                TempData["sucess"] = "Categoria Creado";
                return RedirectToAction("Index");
            }
            return View(categoryObj);
        }


        //Edit
        public IActionResult Edit(int? id)
        {
            if(id == null || id == 0)
            {
                return NotFound();
            }
            var categoryId = _db.Categoriee.Find(id);
            if(categoryId == null)
            {
                return NotFound();
            }
            return View(categoryId);
        }
        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Category categoryObj)
        {
            if (categoryObj.Name == categoryObj.DisplayOrder.ToString())
            {
                ModelState.AddModelError("CustomError", "La orden no puede ser exacto como el nombre");
            }
            if (ModelState.IsValid)
            {
                _db.Categoriee.Update(categoryObj);
                _db.SaveChanges();
                TempData["sucess"] = "Categoria actualizada";
                return RedirectToAction("Index");
            }
            return View(categoryObj);
        }

        //Delete
        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var categoryId = _db.Categoriee.Find(id);
            if (categoryId == null)
            {
                return NotFound();
            }
            return View(categoryId);
        }
        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(Category categoryObj)
        {
            if (categoryObj.Name == categoryObj.DisplayOrder.ToString())
            {
                ModelState.AddModelError("CustomError", "La orden no puede ser exacto como el nombre");
            }
            if (ModelState.IsValid)
            {
                _db.Categoriee.Remove(categoryObj);
                _db.SaveChanges();
                TempData["error"] = "Categoria Eliminada";
                return RedirectToAction("Index");
            }
            return View(categoryObj);
        }
    }
}
