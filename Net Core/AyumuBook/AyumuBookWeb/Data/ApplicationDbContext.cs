﻿using AyumuBookWeb.Models;
using Microsoft.EntityFrameworkCore;

namespace AyumuBookWeb.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<Category> Categoriee { get; set; }
    }
}