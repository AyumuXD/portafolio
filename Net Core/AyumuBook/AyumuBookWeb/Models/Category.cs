﻿using MvcContrib.UI.InputBuilder.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace AyumuBookWeb.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [DisplayName("DisplayOrder")]
        [Range(1,100,ErrorMessage = "No puede superar de 100")]
        public int DisplayOrder { get; set; }
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;
    }
}