﻿using Microsoft.AspNetCore.Mvc;
using Portafolio.Models;
using Portafolio.Servicio;
using System.Diagnostics;

namespace Portafolio.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRepositorioProyectos repositorioProyectos;

        public HomeController(ILogger<HomeController> logger, IRepositorioProyectos repositorioProyectos)
        {
            _logger = logger;
            this.repositorioProyectos = repositorioProyectos;
        }

        public IActionResult Index()
        {
            var proyecto = repositorioProyectos.obtenerProyecto().Take(3).ToList();
            var moddelo = new HomeIndex() { Proyectos = proyecto };
            return View(moddelo);
        }
        

        public IActionResult Proyectos()  
        {
            var proyecto = repositorioProyectos.obtenerProyecto();
            return View(proyecto);
        }
        public IActionResult Contacto()
        {
            var proyecto = repositorioProyectos.obtenerProyecto();
            return View();
        }
        [HttpPost]
        public IActionResult Contacto(Contacto contacto)
        {
            return RedirectToAction("Gracias");
        }
        public IActionResult Gracias()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}