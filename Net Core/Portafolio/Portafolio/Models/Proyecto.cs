﻿namespace Portafolio.Models
{
    public class Proyecto
    {

        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public String ImagenUrl { get; set; }
        public String Link { get; set; }
    }
}
