﻿using Portafolio.Models;

namespace Portafolio.Servicio
{
    public interface IRepositorioProyectos
    {
        List<Proyecto> obtenerProyecto();
    }
}
