﻿using Portafolio.Models;

namespace Portafolio.Servicio
{
    public class RepositorioProyectos : IRepositorioProyectos
    {
        public List<Proyecto> obtenerProyecto()
        {
            return new List<Proyecto>() { new Proyecto
            {
                Titulo =  "Amazon",
                Descripcion = "E-comerce realizado en ASP.NET CORE",
                Link = "https://amazon.com",
                ImagenUrl = "/images/laravel.jpg"
            },new Proyecto
            {
                Titulo =  "Java",
                Descripcion = "Cuenta de persona",
                Link = "https://amazon.com",
                ImagenUrl = "/images/JavaXD.png"
            },new  Proyecto
            {
                Titulo =  "Js",
                Descripcion = "Corte salon",
                Link = "https://amazon.com",
                ImagenUrl = "/images/js.jpg"
            },

            };
        }
    }
}
